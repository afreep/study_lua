-- 単純選択法（選択ソート）
-- 一番小さなデータを選択して、先頭から順に並べていく。
-- ①探索範囲の最小値を探す処理
-- ②探索範囲の最小値を先頭要素と交換する処理

local array = {12, 13, 11, 14, 10}

local i = 1
local k
local w
local indexMin

while i<5 do
    indexMin = i
    k = i+1
    while k<6 do
        if array[k]<array[indexMin] then
            indexMin = k
        end
        k = k+1
    end

    w = array[i]
    array[i] = array[indexMin]
    array[indexMin] = w
    i = i+1
end

print("先頭から順に要素のデータを出力する\n")
for i=1,5 do
    print(array[i])
end
