-- ハッシュ探索法
-- 探索しやすいように予め関数を使ってデータを格納しておく。
-- 格納するのに使った関数を使い、一発でデータを探索する。

arrayD = {12, 25, 36, 20, 30, 8, 42}
arrayH = {}
for i=0,10 do
    arrayH[i] = 0
end

local i = 1
local k

local function hash(data)
    k = data % 11
    return k
end

while i<8 do
    hash(arrayD[i])
    while arrayH[k] ~= 0 do
        local nextNum = k+1
        hash(nextNum)
    end
    arrayH[k] = arrayD[i]
    i = i+1
end

print("=====データが格納されました=====\n")
print("探索する数字を入力してください。\n================")
local findingNum = io.read("*n")

k = hash(findingNum)

while arrayH[k] ~= 0 do
    if arrayH[k] == findingNum then
        break
    else
        local nextNum = k+1
        k = hash(nextNum)
    end
end

if arrayH[k] == 0 then
    print(findingNum.."は存在しません。")
else
    print("格納場所は"..k.."番目の要素です。")
end
