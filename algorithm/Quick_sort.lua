-- クイックソート
-- 基準値を境にしてデータを大小に分ける処理を副プログラムとして繰り返し利用する。

local array = {5, 4, 7, 6, 8, 3, 1, 2, 9}
local left = 1
local right = 9
local i
local k
local w

local function quickSort(array, left, right)
    i = left+1
    k = right
    while i<k do
        while array[i]<array[left] and i<right do
            i = i+1
        end

        while array[k]>=array[left] and k>left do
            k = k-1
        end

        if i < k then
            -- i,kを交換
            w = array[i]
            array[i] = array[k]
            array[k] = w
        end
    end

    -- 真ん中の値と交換
    if array[left] > array[k] then
        w = array[left]
        array[left] = array[k]
        array[k] = w
    end

    if left < k-1 then
        quickSort(array, left, k-1)
    end

    if k+1 < right then
        quickSort(array, k+1, right)
    end

    return array
end
quickSort(array, left, right)

print("======================")
print("先頭から順に要素のデータを出力")
for i=1,9 do
    print(array[i])
end
