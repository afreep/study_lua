-- エラトステネスのふるい
-- 素数を求めるアルゴリズム

local array = {}

-- 初期化
for i=1,100 do
    array[i] = 1
end

local k = 2

while k*k <= 100 do
    i = k
    while i <= 100/k do
        array[k*i] = 0
        i = i+1
    end

    k = k+1
    while array[k] == 0 do
        k = k+1
    end
end

i = 2
while i <= 100 do
    if array[i] == 1 then
        print(i)
    end
    i = i+1
end
