-- 線形探索法のアルゴリズム
-- 配列内から入力された数字を先頭から順に探索する。

local array = {4,2,3,5,1}

print("数字を入力してください\n====================")
local inputVariable = io.read("*n")

for i=1, 5 do
    if array[i] == inputVariable then
        print(i .. "番目の要素が一致しました。")
        break
    elseif i == 5 then
        print("見つかりませんでした。")
    end
end
