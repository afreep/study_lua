-- 単純挿入法
-- 昇順に並び替える。

local array = {5, 3, 4, 1, 2}

local i = 2
local k
local x

while i<6 do
    x = array[i]
    k = i
    while k>1 and array[k-1]>x do
        array[k] = array[k-1]
        k = k-1
    end
    array[k] = x
    i = i+1
end

print("先頭から順に、要素のデータを出力して終了")
print("=================================")
for i=1,5 do
    print(array[i])
end
