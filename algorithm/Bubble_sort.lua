-- 単純交換法（バブルソート）
-- 隣り合ったデータを交換する処理を繰り返して全体を整列する。
-- 昇順に並び替える。
-- 右端の要素から順に、隣あったデータを昇順に並び替える。

local array = {5, 3, 4, 1, 2}

local i
local k = 1
local w

while k<5 do
    i = 5
    while i>k do
        if array[i-1] > array[i] then
            w = array[i-1]
            array[i-1] = array[i]
            array[i] = w
        end
        i = i-1
    end
    k = k+1
end

print("先頭から順に要素のデータを出力して終了\n")
print("===============================")
for i=1,5 do
    print(array[i])
end
