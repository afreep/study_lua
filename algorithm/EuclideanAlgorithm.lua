-- ユークリッドの互除法
-- 最大公約数を求めるアルゴリズム
-- 割られる数を次のループで割る数に設定し、余りを割られる数にする。
-- 最終的にあまりが0になった時、割った数が最大公約数となる。
-- ここでいうｂに当てはまる。

-- 前判定
print("\n<前判定型反復構造>\n")
print("数字を入力してください")
local a = io.read('*n')
print("数字を入力してください")
local b = io.read('*n')

local r = a%b

while r ~= 0 do
    a = b
    b = r

    r = a%b
end

print("最大公約数は、"..b.."です")

-- 後判定
print("\n<後判定型反復構造>\n")
print("数字を入力してください")
local x = io.read('*n')
print("数字を入力してください")
local y = io.read('*n')
local z

while z ~= 0 do
    z = x%y
    x = y
    y = z
end

print("最大公約数は、"..x.."です")
