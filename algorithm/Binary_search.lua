-- 二分探索法のアルゴリズム
-- 予め昇順か降順に整列されているデータが対象
-- 探索する範囲を半分に絞りながら探索を進める。

local array = {11,13,17,19,23,29,31}

for i=1,7 do
    print("配列["..i.."]"..array[i])
end

print("探す数字を入力してください\n====================")
local findingNum = io.read("*n")

local head = 1
local tail = 7
local center = (head+tail)/2

while array[center] ~= findingNum do
    if head <= tail then
        if array[center] < findingNum then
            head = center+1
        else
            tail = center-1
        end
        center = (head+tail)/2
    else
        break
    end
end

if head <= tail then
    print(center .. "番目の要素が一致しました。")
else
    print("見つかりませんでした。")
end
