-- 1_1 チャンク
-- 【チャンク】実行される各コード（ファイルや、対話モードでの１行１行など）のこと

local a = 1
local b = a*2

local a = 1;
local b = a*2;

local a = 1; b = a*2
local a = 1 b = a*2

-- 全て有効である。
