print(4 and 5) --> 5
print(nil and 13) --> nil
print(false and 13) ---> false
print(4 or 5) --> 4
print(false or 5) --> 5

-- 【覚えた！！】Luaで良く使われるイディオム
x = x or v
-- x が false/nil の場合にはxにvが代入される。xが真の場合はそのまま。
-- これは下記のコードと同義である
if not x then
    x = v
end

print("=====================")
x = 2
y = 3
max = (x > y) and x or y
print("x : "..x)
print("y : "..y)
print(max)
-- x > yが真であれば、andの左項となる式は真。andの評価結果は右項の式（ｘ）となる。
-- コレは常に真なので、or式の評価結果はその左項であるxとなる。
-- x > yが偽であれば、and式は偽となり、orの評価結果は右項つまりyとなる。
print("=====================")
print(not nil)
print(not false)
print(not 0)
print(not not nil)
