-- luaは動的型付け言語である。
-- 各値は独自の型を持つ

print(type("Hello World"), "Hello World") --> string　文字列
print(type(10.4*3), 10.4*3) --> number　数値
print(type(print), print) --> function 関数
print(type(type), type) --> function 関数
print(type(true), true) --> boolean ブール値
print(type(nil), nil) --> nil
print(type(type(X))) --> string Xの値に関係無く、常に"string"となる。戻り値が常に文字列であるから。
print("=====================================")
print(type(a), a) --> nil
a = 10
print(type(a), a) --> number
a = "a string"
print(type(a), a) --> string
a = print
a(type(a), a) --> function
