-- 文字の変更を行うと、文字の変更結果を含んだ新しい文字列が作成される。

a = "one string"
b = string.gsub(a, "one", "another") -- 文字列の一部を変更
print(a)
print(b)

-- ----------------------------------------------------------
-- \a : ベル
-- \b : バックスペース
-- \f : フォームフィード
-- \n : 改行
-- \r : キャリッジリターン
-- \t : 水平タブ
-- \v : 垂直タブ
-- \\ : バックスラッシュ
-- \" : ダブルクォート
-- \' : シングルクォート
-- ----------------------------------------------------------
print("\n例題")
print("one line\nnext line\n\"in quotes\", 'in quotes'")
print("\n例題")
print('a backslash inside quotes: \'\\\'')
print("\n例題")
print("a simpler way: '\\'")

print("\n")
print("10" + 1) --> 11
print("10 + 1") --> 10 + 1
print("-5.3e-10"*"2") --> -1.062-09

print("\n")
print(type(10 .. 20), 10 .. 20)

print("\n")
print("数値を入力すると二乗される。それ以外はエラー")
print("\n")
line = io.read()
n = tonumber(line)
if n == nil then
    error(line .. " is not a valid number")
else
    print(n*2)
end

print("\n")
print("数値を文字列に変換")
print("\n")
print(tostring(10) == "10") --> true
print(10 .. ""== "10") --> true
print(10 == "10") --> false

print("\n")
print("長さを調べる")
print("\n")
a = "hello"
print(#a, "hello")
print(#"good\0bye", "good\0bye")
