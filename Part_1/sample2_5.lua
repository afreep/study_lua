a = {}
k = "x"
a[k] = 10 -- 新しいエントリ。キーは"x"で、値は10
a[20] = "great" -- 新しいエントリ。キーは20で、値は"great"
print(a["x"]) --> 10
k = 20
print(a[k]) --> "great"
a["x"] = a["x"] +1 --エントリ"x"をインクリメント
print(a["x"]) --> 11

print("=================")
a = {}
a["x"] = 10
b = a --> bはaが参照するテーブルと同じテーブルを参照
print(b["x"]) --> 10
b["x"] = 20
print(a["x"]) --> 20
a = nil -- この時点でテーブルを参照しているのはbのみ
b = nil -- テーブルを参照する変数はありません

print("=================")
a = {} -- 空のテーブル
-- 新規エントリを1000個作成
for i = 1, 1000 do a[i] = i*2 end
print(a[9]) --> 18
a["x"] = 10
print(a["x"]) --> 10
print(a["y"]) --> nil

print("=======シンタックスシュガー==========")
a.x = 10
print(a.x)
print(a.y)

print("===================")
a = {}
x = "y"
a[x] = 10
print(a[x]) --> 10
print(a.x) --> nil
print(a.y) --> 10

print("===================")
print("入力してね")
a = {}
for i = 1, 10 do
    a[i] = io.read()
end
-- 行の出力
print("出力結果⬇")
for i = 1, #a do
    print(a[i])
end

print("===================")
print("長さの演算子を使ったイディオム")
print("===================")
print("リストの最後の要素を出力")
print(a[#a]) -- リストaの最後の要素を出力
a[#a] = nil -- 最後の値を削除
a[#a+1] = v -- リストの末尾にvの値を追加
print("入力してね")
a = {}
for i = 1, 10 do
    a[#a+1] = io.read()
end
print("出力結果⬇")
for i = 1, #a do
    print(a[i])
end
