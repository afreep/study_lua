local Story = {}

local data = {}

local rawData = json.decode(readText(JsonDir .. 'story.json', system.ResourceDirectory))

function Story.getList( chapterId )
	return data[tonumber(chapterId)]
end

function Story.getData( chapterId, id )
	return data[tonumber(chapterId)][tonumber(id)]
end

for _, story in pairs (rawData) do
    if not data[story[2]] then
        data[story[2]] = {}
    end

    if not data[story[2]][story[3]] then
        data[story[2]][story[3]] = {}
    end

    if not data[story[2]][story[3]]['id'] then
        data[story[2]][story[3]]['id'] = {}
    end

    if not data[story[2]][story[3]]['chapter'] then
        data[story[2]][story[3]]['chapter'] = {}
    end

    if not data[story[2]][story[3]]['storyId'] then
        data[story[2]][story[3]]['storyId'] = {}
    end

    if not data[story[2]][story[3]]['charaName'] then
        data[story[2]][story[3]]['charaName'] = {}
    end

    if not data[story[2]][story[3]]['text'] then
        data[story[2]][story[3]]['text'] = {}
    end

    if not data[story[2]][story[3]]['charaImg'] then
        data[story[2]][story[3]]['charaImg'] = {}
    end

    if not data[story[2]][story[3]]['bgImg'] then
        data[story[2]][story[3]]['bgImg'] = {}
    end

    table.insert( data[story[2]][story[3]]['id'], story[1] )
    table.insert( data[story[2]][story[3]]['chapter'], story[2] )
    table.insert( data[story[2]][story[3]]['storyId'], story[3] )
    table.insert( data[story[2]][story[3]]['charaName'], story[4] )
    table.insert( data[story[2]][story[3]]['text'], story[5] )
    table.insert( data[story[2]][story[3]]['charaImg'], story[6] )
    table.insert( data[story[2]][story[3]]['bgImg'], story[7])
end
return Story
