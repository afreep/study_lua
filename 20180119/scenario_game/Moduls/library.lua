function writeText(name, data, dir)
    -- print("library.lua  - - - - -  writeText - - - - - ")
    local directory = dir or system.DocumentsDirectory
    local path = system.pathForFile(name, directory)

    local file = io.open(path, "w")
    if file then
        file:write(data)
        io.close(file)
    else -- ファイルがない場合は,ファイルを作成してテキストを書き込む
        local directoryName = ""
        for k, v in string.gmatch(name, "(%w+)/") do
            directoryName = directoryName..k.."/"
            createDirectory(directoryName,directory)
        end
        --writeTextライブラリが何重にもの呼び出される恐れがあるため一回のみ行う
        file = io.open(path, "w")
        if file then
            file:write(data)
            io.close(file)
        end
    end
end

function readText(name, dir)
    local directory = dir or system.DocumentsDirectory
    local path = system.pathForFile(name, directory)
    local file = io.open(path, "r")
    if file then
        -- Reads the whole file, starting at the current position. On end of file, it returns the empty string.
        local contents = file:read("*a")
        return contents
    else
        return nil
    end
end
