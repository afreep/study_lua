local Story = {}

function Story.new(group)
    local story = display.newGroup()
    group:insert(story)

    local function finishStory(story)
        if story == nil or story.data == nil then return true end
        if story.page > #story.data.text then
            composer.gotoScene( ContDir .. 'start')
            return true
        end
        return false
    end

    function story:create()
        self.group = display.newGroup()
        self:insert(self.group)

        self.bg = display.newRect(self.group, CENTER_X, CENTER_Y, _W, _H)
        self.bg:setFillColor(0)

        self.inner = display.newGroup()
        self.group:insert(self.inner)

        -- layerを分ける。
        -- 奥から順に、「背景」「キャラ」
        self.layer = {}
        for i = 1, 2 do
            self.layer[i] = display.newGroup()
            self.inner:insert(self.layer[i])
        end

        self.textArea = display.newImage(self.group, ImgDir .."UI/story/popup.png", CENTER_X, CENTER_Y+380)

        self.outer = display.newGroup()
        self.group:insert( self.outer )

        self.page = 1
        self:setContent()
    end

    function story:setContent()
        if finishStory( self ) then
            return
        end
        print( '全ページ数', #self.data.text )
        print( '現在のページ数', self.page )

        local function showStoryContent()
            if self.text then
                display.remove( self.text )
                self.text = nil
            end

            if self.name then
                display.remove( self.name )
                self.name = nil
            end

            if self.next then
                display.remove( self.next )
                self.next = nil
            end

            if self.data.bgImg[self.page] and self.data.bgImg[self.page] ~= "" then
                if self.storyBg then
                    display.remove( self.storyBg )
                    self.storyBg = nil
                end
                self.storyBg = display.newImage( self.layer[1], ImgDir .. 'bg/' ..self.data.bgImg[self.page], 0, CENTER_Y )
                self.storyBg.anchorX = 0
            end

            if self.data.charaImg[self.page] and self.data.charaImg[self.page] ~= "" then
                if self.chara then
                    display.remove( self.chara )
                    self.chara = nil
                end
                self.chara = display.newImage( self.layer[2], ImgDir .. 'chara/' ..self.data.charaImg[self.page], 0, CENTER_Y )
                self.chara.anchorX = 0
            end

            if self.data.charaName[self.page] then
                self.name = display.newText{
                    parent = self.outer,
                    text = self.data.charaName[self.page],
                    x = 236,
                    y = _H-339,
                    font = NormalFont,
                    fontSize = 24,
                }
            end

            local content = self.data.text[self.page]
            self.text = display.newText{
                parent = self.outer,
                text = self.data.text[self.page],
                x = 320,
                y = _H-260,
                width = _W-120,
                font = NormalFont,
                fontSize = 24,
                align = 'left'
            }

            self.page = self.page + 1
        end

        showStoryContent()
    end

    function story:destroy()
        display.remove( self.group )
        self.group = nil
    end

    function story:tap(event)
        self:setContent()
        return true
    end
    story:addEventListener('tap')

    return story
end

return Story
