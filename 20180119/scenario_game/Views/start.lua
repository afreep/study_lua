local Start = {}

function Start.new (group)
    local start = display.newGroup()
    group:insert( start )

    function start:show()
        self.inner = display.newGroup()
        self:insert( self.inner )

        local bg = display.newImage( self.inner, ImgDir .. 'UI/start/bg.png', CENTER_X, CENTER_Y )

        local saishokara = display.newImage(self.inner, ImgDir .. 'UI/start/saishokara.png', CENTER_X, CENTER_Y)
        saishokara:addEventListener( 'tap', function()
            composer.gotoScene( ContDir .. 'story', { params = { chapterId = 1, id = 1 } } )
        end)

        local saishokara = display.newImage(self.inner, ImgDir .. 'UI/start/saishokara.png', CENTER_X, CENTER_Y+200)
        saishokara:addEventListener( 'tap', function()
            composer.gotoScene( ContDir .. 'story', { params = { chapterId = 1, id = 2 } } )
        end)

        local saishokara = display.newImage(self.inner, ImgDir .. 'UI/start/saishokara.png', CENTER_X, CENTER_Y+400)
        saishokara:addEventListener( 'tap', function()
            composer.gotoScene( ContDir .. 'story', { params = { chapterId = 2, id = 1 } } )
        end)
    end
    return start
end

return Start
