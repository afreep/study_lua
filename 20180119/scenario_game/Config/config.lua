--ステータスバーの設定
-- display.setStatusBar( display.TranslucentStatusBar )
display.setStatusBar( display.HiddenStatusBar )

_W  = display.contentWidth
_H  = display.actualContentHeight

CENTER_X = display.contentCenterX
CENTER_Y = display.contentCenterY

--呼び出すディレクトリ
ImgDir     = 'Assets/Images/'
ViewDir    = "Views."
ModelDir   = "Models."
ModDir     = "Moduls."
ContDir    = "Cont."
AudioDir   = "Assets/Audios/"
JsonDir    = "Jsons/"

require(ModDir .. "library")
require(ModDir .. "print")

composer = require "composer"
json = require "json"
