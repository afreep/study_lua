local scene = composer.newScene()

local StoryModel = require "Models.story"
local StoryView = require "Views.story"

local views = {}

function scene:create( event )
    local sceneGroup = self.view
    views.story = StoryView.new(sceneGroup)
end

function scene:show( event )
    local sceneGroup = self.view
    local phase = event.phase
    local params = event.params

    if ( phase == "will" ) then
        views.story.data = StoryModel.getData(params.chapterId, params.id)

        views.story:create()
    elseif ( phase == "did" ) then

    end
end


-- hide()
function scene:hide( event )
    local sceneGroup = self.view
    local phase = event.phase
    if ( phase == "will" ) then

    elseif ( phase == "did" ) then
        views.story:destroy()
    end
end


-- destroy()
function scene:destroy( event )
    local sceneGroup = self.view
    for key, _ in pairs(views) do
        views[key] = nil
    end
end


-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------

return scene
