local scene = composer.newScene()

local StartView = require "Views.start"

local views = {}

function scene:create( event )
    local sceneGroup = self.view
    views.startView = StartView.new(sceneGroup)
end

function scene:show( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then
        views.startView:show()
    elseif ( phase == "did" ) then

    end
end


-- hide()
function scene:hide( event )
    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then
    elseif ( phase == "did" ) then
    end
end


-- destroy()
function scene:destroy( event )
    local sceneGroup = self.view
    for key, _ in pairs( views ) do
        views[key] = nil
    end
end


-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------

return scene
